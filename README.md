# Interfaccia CLI per la piattaforoma

## Description

Semplice interfaccia CLI per agire via API sulla stanza del cittadino.

Al momento solo le API delle transitions delle applications sono supportate

## Installation

Requirements:

* Python 3
* dependencies specified in `requirements.txt`

## Usage

```
usage: sdc-application.py [-h] --service-slug SERVICE_SLUG --username USERNAME
                          --password PASSWORD --api-base-url API_BASE_URL
                          --current-status CURRENT_STATUS --transition
                          TRANSITION [--verbose]

Interfaccia command-line per le Applications della piattaforma stanza del
cittadino

optional arguments:
  -h, --help            show this help message and exit
  --service-slug SERVICE_SLUG
                        le pratiche sono sempre filtrate per un servizio,
                        identificato mediante lo slug del servizio (la parte
                        finale della URL della pagina del servizio, derivata
                        dal titolo dello stesso
  --username USERNAME   l'username di un operatore che ha accesso al servizio
  --password PASSWORD   la password dell'operatore
  --api-base-url API_BASE_URL
                        l'URL API base del tenant (es:
                        https://qa.stanzadelcittadino.it/comune-di-
                        bugliano/api)
  --current-status CURRENT_STATUS
                        La ricerca è limitata alle pratiche in questo stato
                        (per un elenco degli stati disponibili consultare la
                        pagina https://gitlab.com/opencontent/stanza-del-
                        cittadino/core/-/wikis/Informazioni-per-
                        sviluppatori/Gli-stati-delle-pratiche
  --transition TRANSITION
                        La transizione che si vuole eseguire sulla pratica: ad
                        esempio "accept" o "refuse" (nota bene: prima della
                        transizione viene sempre eseguita una assign in modo
                        che l'operatore usato abbia in carico la pratica)
  --verbose             Rende più verboso lo script

```

## Support

## Contributing

Sentitevi liberi di contribuire facendo una MR per inviare correzioni o nuove API supportate.

## Authors and acknowledgment
 
Original developer: Andrea Bruno <andrea.bruno@opencitylabs.it>

Contributors:
 - Lorenzo Salvadorini <lorenzo.salvadorini@opencontent.it>

## License

Licensed under GNU GPL v3, see LICENSE file

## Project status

Stable and used in production

