#!/usr/bin/env python3

import argparse
import logging
import requests
import errno

# create an instance of the logger
logger = logging.getLogger()
# logger set up 
log_format = logging.Formatter('%(asctime)-15s %(levelname)-2s %(message)s')
sh = logging.StreamHandler()
sh.setFormatter(log_format)

# add the handler
logger.addHandler(sh)
logger.setLevel(logging.INFO)

parser = argparse.ArgumentParser(description='Interfaccia command-line per le Applications della piattaforma stanza del cittadino')
parser.add_argument('--service-slug',type=str, required=True, help='le pratiche sono sempre filtrate per un servizio, identificato mediante lo slug del servizio (la parte finale della URL della pagina del servizio, derivata dal titolo dello stesso')
parser.add_argument('--username',type=str, required=True, help='l\'username di un operatore che ha accesso al servizio')
parser.add_argument('--password',type=str, required=True,help='la password dell\'operatore') 
parser.add_argument('--api-base-url',type=str, required=True, help='l\'URL API base del tenant (es: https://qa.stanzadelcittadino.it/comune-di-bugliano/api)')
parser.add_argument('--current-status',type=str, required=True, help='La ricerca è limitata alle pratiche in questo stato (per un elenco degli stati disponibili consultare la pagina https://gitlab.com/opencontent/stanza-del-cittadino/core/-/wikis/Informazioni-per-sviluppatori/Gli-stati-delle-pratiche')
parser.add_argument('--transition',type=str, required=True, help='La transizione che si vuole eseguire sulla pratica: ad esempio "accept" o "refuse" (nota bene: prima della transizione viene sempre eseguita una assign in modo che l\'operatore usato abbia in carico la pratica)')
parser.add_argument('--verbose', default=False, action='store_true', help='Rende più verboso lo script')
opt = parser.parse_args()

if opt.verbose:
    logger.setLevel(logging.DEBUG)

try:
    response = requests.post(f"{opt.api_base_url}/auth",json = {"username": opt.username,"password": opt.password})
    response.raise_for_status()
    logger.info(f"Loggedin with user '{opt.username}' on {opt.api_base_url}")
except requests.exceptions.HTTPError as error:
    logger.error(f"Error trying to login with user '{opt.username}' on API '{opt.api_base_url}': {error}")
    exit(errno.ENODATA)

token = response.json()['token']
header = {"Authorization": f"Bearer {token}"}
page = f"{opt.api_base_url}/applications?service={opt.service_slug}&status={opt.current_status}&limit=10"
while (page):
    try:
        response = requests.get(page, headers = header)
        response.raise_for_status()
        logger.info(f"Reading applications page: {page}")
    except requests.exceptions.HTTPError as error:
        logger.error(f"Error getting page '{page}': {error}")
        next
    response_json = response.json()
    # getting next page url from the links of the current page
    nextpage = response_json["links"]["next"]
    if not nextpage:
        # page doesn't change, but nextpage must exist to continue the loop
        page = False 

    #logger.debug(f"Next page read from current page metadata: {page}")
    for application in response_json["data"]:
        id = application["id"]
        logger.debug(f"Checking application {id}")

        # before accepting the application I need to be sure that I'm the operator assigned to application
        try:
            response = requests.post(f"{opt.api_base_url}/applications/{id}/transition/assign", headers = header)
            response.raise_for_status()
            logger.debug(f"Assigning application '{id}' to '{opt.username}'")
        except requests.exceptions.HTTPError as error:
            logger.error(f"Cannot assign application '{id}' to '{opt.username}': {error}")

        transition_link = ""
        for link in application["links"]:
            if link["action"] == opt.transition:
                transition_link = link["url"]
        try:
            response = requests.post(transition_link, headers = header)
            response.raise_for_status()
            logger.info(f"Application '{id}' accepted by operator '{opt.username}'")
        except requests.exceptions.HTTPError as error:
            logger.error(f"Cannot accept application '{id}': {error}")
    # uncomment the following line to check the script on the first page of applications
    # break
logger.info("Finished successfully.")
